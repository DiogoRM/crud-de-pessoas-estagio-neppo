-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21-Nov-2017 às 09:42
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cadastrodeclientes`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `cpf` varchar(11) DEFAULT NULL,
  `idade` int(11) DEFAULT NULL,
  `sexo` varchar(1) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `datanascimento` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `cpf`, `idade`, `sexo`, `endereco`, `datanascimento`) VALUES
(11, 'Diogo Reis Martins', '03685028138', 21, 'M', 'R. HorÃ¡cio N52A', '30/07/1996'),
(12, 'Thalles Viana de Oliveira', '00000000001', 21, 'M', 'R. HorÃ¡cio N52A', '16/04/1996'),
(13, 'Gabriel Humberto de Oliveira', '0000000002', 21, 'M', 'R. HorÃ¡cio N52A', '00/00/00'),
(19, 'Mozart Matos ', '00000000003', 22, 'M', 'R. HorÃ¡cio N52A', '30/05/1995'),
(20, 'Ana EmÃ­lia de Freitas', '00000000004', 21, 'F', 'R. HorÃ¡cio N52A', '03/09/1996'),
(21, 'Patricia Soares', '12345678911', 24, 'F', 'R. HorÃ¡cio N52A', '05/03/1993'),
(22, 'Maria', '00000000001', 3, 'F', 'R. HorÃ¡cio N52A', '01/03/2014'),
(23, 'JoÃ£o', '00000000001', 5, 'M', 'R. HorÃ¡cio N52A', '00/00/00'),
(24, 'Gleibe Maria', '00000000005', 51, 'F', 'R. HorÃ¡cio N52A', '22/12/1966');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
