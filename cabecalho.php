<html 
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Cadastro de Clientes</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="cliente.css" rel="stylesheet">
</head>

<body>
	
	
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="Index.php">Cadastro de Clientes</a>
			</div>
			<div >
				<ul class="nav navbar-nav">
					<li>
						<a href="cliente-formulario.php">Adiciona Cliente</a>
					</li>
					
					<li>
						<a href="cliente-lista.php">Lista de Clientes</a>
					</li>
				
					<li>
						<a href="busca.php">Buscar Cliente</a>
					</li>
					
					<li>
						<a href="grafico.php">Gráficos</a>
					</li>
				
				</ul>
			</div>
		</div>
	</div>
	
	
	
	<div class="container">
		<div class="principal">
			
		