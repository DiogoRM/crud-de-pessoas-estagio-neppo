<?php include("cabecalho.php");
 include("conecta.php");
 include("cliente-funcoes.php"); ?>

<?php
	if(array_key_exists("removido", $_GET) && $_GET["removido"]=="true"){
	?>
		<p class="alert-sucess">Cliente apagado com suceso</p>
<?php
}
?>


<table class="table table-striped table-bordered">
	<?php
		$clientes = listaClientes($conexao);
		foreach($clientes as $cliente):
	?>
	<tr>
		<td><?= $cliente['nome'] ?></td>
		<td><?= $cliente['datanascimento'] ?></td>
		<td><?= $cliente['cpf'] ?></td>
		<td><?= $cliente['sexo'] ?></td>
		<td><?= $cliente['endereco'] ?></td>
		<td><?= $cliente['idade'] ?></td>
		
		<td><a class="btn btn-primary" href="cliente-altera-formulario.php?id=<?=$cliente['id']?>">alterar</a></td>
		
		<td>
			<form action="remove-cliente.php" method="post">
				<input type="hidden" name="id" value="<?=$cliente['id']?>">
				<button class="btn-danger">remover</button>
			</form>
		</td>
		
		<td><a class="btn btn-primary" href="mostra-cliente.php?id=<?=$cliente['id']?>">Mostrar</a></td>
		
		
	</tr>
	<?php
	
	endforeach
    

	?>
</table>	
	
<?php include("rodape.php");?>