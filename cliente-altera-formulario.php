<?php include("cabecalho.php");
include("conecta.php");
include("cliente-funcoes.php");
$id = $_GET['id'];
$cliente = buscaCliente($conexao,$id);
?>

<h1>Alterar cliente:<br><br></h1>

<form action="altera-cliente.php" method="post">
	<input type="hidden" name="id" value="<?=$cliente['id']?>">
	<table class="table">
		
		<tr>
			<td>Nome:</td>
			<td><input class="form-control" type="text" name="nome" value="<?=$cliente['nome']?>"></td>
		</tr>

		<tr>
			<td>Data de Nascimento:</td>
			<td><input class="form-control" type="text"name="datanascimento" value="<?=$cliente['datanascimento']?>"></td>
		</tr>
	
		<tr>
			<td>CPF:</td>
			<td><input class="form-control" type="text"name="cpf" value="<?=$cliente['cpf']?>"></td>
		</tr>
	
		<tr>
			<td>Sexo:</td>
			<td><input class="form-control" type="text"name="sexo" value="<?=$cliente['sexo']?>"></td>
		</tr>
	
		<tr>
			<td>Endereco:</td>
			<td><input class="form-control" type="text"name="endereco" value="<?=$cliente['endereco']?>"></td>
		</tr>
		
		<tr>
			<td>idade:</td>
			<td><input class="form-control" type="text"name="idade" value="<?=$cliente['idade']?>"></td>
		</tr>
		
		<tr>
			<td>
				<button class="btn btn-primary" type="submit">Alterar</button>
			</td>
		</tr>
	
	</table>
</form>
	
	
<?php include("rodape.php");?>