<?php include("cabecalho.php");
include("conecta.php");
include("cliente-funcoes.php");


$cont_feminino = 0;
$cont_masculino = 0;
$cont_0a9 = 0;
$cont_10a19 = 0;
$cont_20ate29 = 0;
$cont_30a39 = 0;
$cont_Maiorque40 = 0;
	
$clientes = grafico($conexao);
foreach($clientes as $cliente){
	
	
	
	if($cliente['sexo'] == 'F')
		$cont_feminino++;
	
	else
		$cont_masculino++;

}

$clientes = grafico($conexao);
foreach($clientes as $cliente){
	
	
	
	if($cliente['idade'] > 0 && $cliente['idade'] <=9)
		$cont_0a9++;
	
	elseif($cliente['idade'] >= 10 && $cliente['idade'] <=19)
		$cont_10a19++;

    elseif($cliente['idade'] >= 20 && $cliente['idade'] <=29)
		$cont_20ate29++;
	
	elseif($cliente['idade'] >= 30 && $cliente['idade'] <=39)
		$cont_30a39++;

	elseif($cliente['idade'] >40)
		$cont_Maiorque40++;
}


?><head>
    
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

		function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Sexo', 'Quantidade'],
          ['Feminino',     <?=$cont_feminino?>],
          ['Masculino',      <?=$cont_masculino?>],
        ]);

        var options = {
          title: 'Sexo'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  





  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Idade', 'Quantidade'],
          ['0 a 9', <?=$cont_0a9?>],
          ['10 a 19', <?=$cont_10a19?>],
          ['20 a 29', <?=$cont_20ate29?>],
          ['30 a 39', <?=$cont_30a39?>],
          ['Maior que 40', <?=$cont_Maiorque40?>]
        ]);

        var options = {
          width: 800,
          legend: { position: 'none' },
          chart: {
            title: 'Gráfico de faixas de idade',
            subtitle: '' },
          axes: {
            x: {
              0: { side: 'top', label: ''} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
    </script>
  </head>
  






  <div class="container">
    <div class="row">
        <div class="col-sm-6"> 
<div id="piechart" align="left" style="width: 500px; height: 500px;"></div>
</div>
<div class="col-sm-6">
<div id="top_x_div" style="width: 500; height: 500;"></div>
 </div>